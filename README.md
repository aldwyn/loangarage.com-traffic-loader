LoanGarage.com Traffic Loader
=============================
[Alpha-release: June 16,2014]
-----------------------------


### Features:

+ built with Python and some other third-party modules
+ runs with Selenium 2 along with Chrome (i.e. a fully JS-capable browser)
+ Chromedriver is an incognito browser in the first place
+ comprises multiprocessing pool of workers to browse multiple target URLs at a time
+ adheres to identity concealment (needs stable high-anonymity proxies)
+ adaptable to code modifications


### Instructions

1. Download Python 2.7.6, and Google Chrome (disregard this step if you already have those).

2. Set-up "app.json". (Carefully, don't modify the "indonesia_proxies" and "singapore_proxies". The app might not run well.)
> worker_counts --> the number of workers you want; if 0, the number of logical processors present in your PC will be the default; it's good to start with 4
> time_limit_for_termination --> the termination time (in minutes), used to stop the execution of the app; good to start with 30 minutes 
> indonesia_setups & singapore_setups --> the core fixed setups for the automated browsing

3. Open "index.py" in the "bin" directory.

4. Enjoy.

> DISCLAIMER: If you wish to terminate the program, open "terminate.bat" from the "bin" directory.


### Contact

>+ Skype: aldwyn_17
>+ Email: aldwyn.up@gmail.com
>+ Contact #: +639238758263
>+ Address: 405-B Sitio Sto. Nino-TESDA, Lahug, Cebu City 6000, Philippines