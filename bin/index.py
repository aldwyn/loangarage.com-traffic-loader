from bs4 import BeautifulSoup
from random import choice
from splinter import Browser
from urlparse import urlparse, parse_qs
from multiprocessing import Process, Queue, cpu_count
import json, time, requests, logging


# DRIVER = 'phantomjs', 'ghost/phantomjs'
DRIVER = 'chrome', 'chrome/chromedriver'
SETTINGS = json.loads(open('app.json').read())
APP_DATA = json.loads(open('settings/index.json').read())
logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')


class Worker(Process):
	def __init__(self, app_data, proxies):
		Process.__init__(self)
		self.search_engine = app_data['search_engine']
		self.target_keywords = app_data['target_keywords']
		self.target_url = app_data['target_url']
		self.proxies = proxies

	def __get_proxy(self):
		while True:
			try:
				return self.proxies.get()
			except:
				time.sleep(5)

	def __visit(self, proxy):
		pxy = ['--proxy='+proxy['proxy_ip'], '--proxy-type='+proxy['proxy_ip']]
		browser = Browser(DRIVER[0], executable_path=DRIVER[1], service_args=pxy, user_agent='Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130401 Firefox/21.0')
		with browser:
			to_search = choice(self.target_keywords)
			browser.visit(self.search_engine)
			browser.fill('q', to_search)
			browser.find_by_name('btnG').click()
			time.sleep(5)
			browser.click_link_by_partial_href(self.target_url)
		print self.name, ':', to_search, 'ON', self.search_engine

	def run(self):
		while True:
			proxy = self.__get_proxy()
			if proxy:
				self.__visit(proxy)
				self.proxies.put(proxy)
			else:
				self.proxies.put(proxy)
				break


def main():
	print 'Traffic loader running...'
	id_pxy = Queue()
	sg_pxy = Queue()
	for pxy in APP_DATA['indonesia_setups']['proxies']:
		id_pxy.put(pxy)
	for pxy in APP_DATA['singapore_setups']['proxies']:
		sg_pxy.put(pxy)
	worker_counts = cpu_count() if not SETTINGS['worker_counts'] else SETTINGS['worker_counts']
	id_workers = [Worker(APP_DATA['indonesia_setups'], id_pxy) for i in xrange(worker_counts)]
	sg_workers = [Worker(APP_DATA['singapore_setups'], sg_pxy) for i in xrange(worker_counts)]
	for worker in id_workers:
		worker.start()
	for worker in sg_workers:
		worker.start()
	time.sleep(SETTINGS['time_limit_for_termination']*60)
	id_pxy.put(None)
	sg_pxy.put(None)
	for worker in id_workers:
		worker.join()
	for worker in sg_workers:
		worker.join()
	print 'Traffic loading successful.'



if __name__ == '__main__':
	main()